import { Component, OnInit } from '@angular/core';
import {DataService} from '../common/service/data.service';
import Game from '../common/models/game.model';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  constructor(private dataService: DataService) { }

  games: Game[];

  ngOnInit() {
    this.dataService.getAllGames().subscribe(data => {
      this.games = data;
    });
  }
}
