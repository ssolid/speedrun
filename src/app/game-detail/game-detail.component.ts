import { Component, OnInit } from '@angular/core';
import { DataService } from '../common/service/data.service';
import { ActivatedRoute } from '@angular/router'
import GameDetails from '../common/models/game-details.model';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  gameDetails: GameDetails;

  ngOnInit() {
    this.route.params.subscribe(params => {
       this.dataService.getAllDetails(params['id'])
         .subscribe(gameDetails => {
           this.gameDetails = gameDetails;
         });
    });
  }
}
