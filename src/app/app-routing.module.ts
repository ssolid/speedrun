import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GameListComponent} from './game-list/game-list.component';
import {GameDetailComponent} from './game-detail/game-detail.component';

const routes: Routes = [
  { path: 'games', component: GameListComponent },
  { path: 'games/:id', component: GameDetailComponent },
  { path: '',
    redirectTo: 'games',
    pathMatch: 'full'
  },
  {path: '**', redirectTo: 'games'} // normally /404
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
