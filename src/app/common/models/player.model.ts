export default class Player {

  id;
  name;
  country = {
    code: '',
    name: ''
  };


  constructor(player: any) {
    this.id = this.getSafe(() => player['id']);
    this.name = this.getSafe(() => player['names']['international']);
    this.country.code = this.getSafe(() => player['location']['country']['code']);
    this.country.name = this.getSafe(() => player['location']['country']['names']['international']);
  }

  getSafe(fn) {
    try {
      return fn();
    } catch (e) {
      return null;
    }
  }
}
