export default class Run {

  id;
  game;
  video;
  date;
  API = {
    player: ''
  };
  time = {
    hours : '',
    minutes : '',
    seconds : ''
  };

  constructor(run: any) {
    this.id = this.getSafe(() => run['id']);
    this.game = this.getSafe(() => run['game']);
    this.video =  this.getSafe(() => run['videos']['links'][0]['uri']);
    this.date = this.getSafe(() => run['date']);
    this.API.player =  this.getSafe(() => run['players'][0]['uri']);

    let tempTime = this.formatTime(this.getSafe(() => run['times']['realtime']));
    if(tempTime){
      this.time.hours = tempTime[0];
      this.time.minutes = tempTime[1];
      this.time.seconds = tempTime[2];
    }
  }

  // convert time format 'PT3H27M16S' to ["3", "27", "16"] : [0] are hours, [1] are minutes, [2] are seconds
  private formatTime(realTime) {
    if (! realTime) return;
    let timeConverted = realTime.replace('PT', '');
    timeConverted = timeConverted.replace('H', ' ');
    timeConverted = timeConverted.replace('M', ' ');
    timeConverted = timeConverted.replace('S', '');
    return timeConverted.split(" ");
  }

  getSafe(fn) {
    try {
      return fn();
    } catch (e) {
      return;
    }
  }
}
