import Game from './game.model';
import Run from './run.model';
import Player from './player.model';
export default class GameDetails {

  game: Game;
  run: Run;
  player: Player;

  constructor(game?: Game, run?: Run, player?: Player) {
    this.game = game;
    this.run = run;
    this.player = player;
  }
}
