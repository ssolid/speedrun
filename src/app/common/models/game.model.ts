export default class Game {

  id;
  name;
  image;
  API = {
    run: ''
  };

  constructor(game: any) {
    this.id = this.getSafe(() => game['id']);
    this.name = this.getSafe(() => game['names']['international']);
    this.image = this.getSafe(() => game['assets']['cover-large']['uri']);

    // we will call this address to get the run information
    // Used to prevent a 404 if a change occurs on the Urls in a future API version.
    this.API.run = game['links'] ? game['links'].find(l => l.rel === 'runs')['uri'] : null;
  }

  getSafe(fn) {
    try {
      return fn();
    } catch (e) {
      return;
    }
  }
}
