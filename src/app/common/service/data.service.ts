import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import Game from '../models/game.model';
import Run from '../models/run.model';
import Player from '../models/player.model';
import GameDetail from '../models/game-details.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private data;
  private api = 'https://www.speedrun.com/api/v1/';
  private observable: Observable<any>;

  constructor(private http: HttpClient) {
  }

  getAllGames(): Observable<Game[]> {
    if (this.data) {
      // if `data` is available just return it as `Observable`
      return of(this.data);
    } else if (this.observable) {
      // if `this.observable` is set then the request is in progress
      // return the `Observable` for the ongoing request
      return this.observable;
    } else {
      // create the request, store the `Observable` for subsequent subscribers
      const params = new HttpParams().set('max', '200');
      this.observable = this.http.get(this.api + 'games', { params: params })
        .pipe(
          map(data => {
            this.data = data['data'].map(game => {
              return new Game(game);
            });
            return this.data;
          })
        );
      return this.observable;
    }
  }

  getGameById(id: string): Observable<Game> {
    this.observable = this.http.get(this.api + 'games/' + id)
      .pipe(
        map(data => {
          return new Game(data['data']);
        })
      );
    return this.observable;
  }

  getFirstRunByGame(game: Game): Observable<Run> {
    this.observable = this.http.get(game.API.run)
      .pipe(
        map(data => {
          return new Run(data['data'][0]);
        })
      );
    return this.observable;
  }

  getPlayerByRun(run: Run): Observable<Player> {
    this.observable = this.http.get(run.API.player)
      .pipe(
        map(data => {
          return new Player(data['data']);
        })
      );
    return this.observable;
  }

  getAllDetails(gameId: string): Observable<any> {
    let gameDetails = new GameDetail();
    return this.getGameById(gameId).pipe(
      mergeMap(game => {
        gameDetails.game = game;
        return this.getFirstRunByGame(game);
      }),
      mergeMap(run => {
        gameDetails.run = run;
        return this.getPlayerByRun(run);
      }),
      mergeMap(player => {
        gameDetails.player = player;
        return of(gameDetails);
      })
    );
  }
}
